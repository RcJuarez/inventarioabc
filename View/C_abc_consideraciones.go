package View

import (
	"strings"
)

type Consideraciones struct {
	FRP bool
	ST  bool
	SV  bool
}

func CrearConsideraciones(consArr []string) Consideraciones {
	var consideraciones Consideraciones
	for i := 0; i < len(consArr); i++ {
		if strings.Compare(consArr[i], "FRP") == 0 {
			consideraciones.FRP = true
		}
		if strings.Compare(consArr[i], "ST") == 0 {
			consideraciones.ST = true
		}
		if strings.Compare(consArr[i], "SV") == 0 {
			consideraciones.SV = true
		}
	}
	return consideraciones
}
