package View

import (
	"strings"

	"../Model"
)

type Fechas struct {
	FechaMin       string
	FechaMax       string
	FechaMinActual string
	FechaMaxActual string
}

func CrearFechas(filtro Model.Filtro) Fechas {
	return Fechas{
		strings.Split(filtro.FechaInicio.String(), " ")[0],
		strings.Split(filtro.FechaFinal.String(), " ")[0],
		strings.Split(filtro.FechaInicio.String(), " ")[0],
		strings.Split(filtro.FechaFinal.String(), " ")[0],
	}
}
