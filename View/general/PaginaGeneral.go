package general

type Mensaje struct {
	Tiene   bool
	Tipo    string
	Mensaje string
	Origen  string
}

type Paginacion struct {
	Tiene          bool
	ValorPrevio    int
	ValorSiguiente int
	TienePrevio    bool
	TieneSiguiente bool
}

type Pagina struct {
	Mensaje    Mensaje
	Contenido  interface{}
	Paginacion Paginacion
}

type AlmacenVista struct {
	Clave        string
	Seleccionado bool
}
