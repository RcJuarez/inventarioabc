package View

import (
	"strings"
)

type Almacen struct {
	Almacen       string
	NombreAlmacen string
	Seleccionado  bool
}

type ListaAlmacen []Almacen

func CrearLista(TodosAlmacenes [][2]string, AlmacenesSeleccionados []string) []Almacen {
	var listaAlmacen ListaAlmacen
	for i := 0; i < len(TodosAlmacenes); i++ {
		var almacen Almacen
		var aux bool
		for j := 0; j < len(AlmacenesSeleccionados); j++ {
			if strings.Compare(TodosAlmacenes[i][0], AlmacenesSeleccionados[j]) == 0 {
				aux = true
				break
			}
		}
		almacen = Almacen{
			TodosAlmacenes[i][0],
			TodosAlmacenes[i][1],
			aux,
		}
		listaAlmacen = append(listaAlmacen, almacen)
	}
	return listaAlmacen
}
