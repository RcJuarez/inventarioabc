package Model

import (
	"strings"
)

/*
	@func PorcentajeAportacionMarginal calcula el porcentaje de la aportacion en pesos
	de un producto contra todos los productos
	@param sumaTotalAportacionProductos es el total de aportacion de todos los productos
*/
func (listaFr *ListaFrecuencia) PorcentajeAportacionMarginal(sumaTotalAportacionProductos float64) float64 {
	aportacionMarginalPesosProducto := (sumaTotalAportacionProductos * 100) / listaFr.CalcularAportacionVentas()
	return aportacionMarginalPesosProducto
}

/*
	@func CalcularAportacionVentas calculamos la cantidad total de aportacion
	monetaria sobre la lista de un producto.
	@return Regresa la aportacion total
*/
func (listaFr *ListaFrecuencia) CalcularAportacionVentas() float64 {
	var aportacionVentasSobreProducto float64
	listaFr.Recorrer(func(fr Frecuencia) {
		aportacionVentasSobreProducto += fr.SumaPrecioVentas
	})
	return aportacionVentasSobreProducto
}

/*
	@func CalcularPromedio
	sobre un producto permite obtener el promedio de sus ventas en un rango
	de fechas
*/
func (listaF *ListaFrecuencia) CalcularPromedio(diastotales int) float64 {
	return float64(listaF.CalcularTotalProductos()) / float64(diastotales)
}

/*
	@func CalcularPorcentajeTiquetsVendidos
	Permite Obtener el porcentaje de los tiquets sobre un producto contra todos los
	productos el total de productos es un parametro externo, que define un conjunto variable
	@param totalProductosVendidos define el total de productos en un momento dado
	@return regresa el porcentaje
*/
func (listaFr *ListaFrecuencia) CalcularPorcentajeTiquetsVendidos(totalProductosVendidos int) float64 {
	return (listaFr.CalcularSumaVentas() * 100) / float64(totalProductosVendidos)
}

/*
	@func CalcularSumaVentas
	sobre un producto obetiene la suma de sus ventas en un rango de fechas
*/
func (listaF *ListaFrecuencia) CalcularSumaVentas() float64 {
	var sumaTiquet float64
	listaF.Recorrer(func(fr Frecuencia) {
		sumaTiquet += fr.Frecuencia
	})
	return sumaTiquet
}

func (listaFr *ListaFrecuencia) CalcularPorcentajeFrecuenciaDiaria(diasTotales int) float64 {
	porcentaje := float64((listaFr.CalcularFrecuenciaDiaria() * 100) / diasTotales)
	return porcentaje
}

/*
	@func CalcularFrecuenciaDiaria
	sobre un producto obtenemos la suma de las frecuencias diarias
*/
func (listaF *ListaFrecuencia) CalcularFrecuenciaDiaria() int {
	fechas := make([]string, 0)
	listaF.Recorrer(func(fr Frecuencia) {
		if fr.Frecuencia > 0 {
			if len(fechas) == 0 {
				fechas = append(fechas, strings.Split(fr.Fecha.String(), " ")[0])
			} else {
				var yaExiste bool
				for i := 0; i < len(fechas); i++ {
					if strings.Compare(fechas[i], strings.Split(fr.Fecha.String(), " ")[0]) == 0 {
						yaExiste = true
					}
				}
				if !yaExiste {
					fechas = append(fechas, strings.Split(fr.Fecha.String(), " ")[0])
				}
			}
		}
	})
	return len(fechas)
}

func (listaF *ListaFrecuencia) CalcularTotalProductos() int {
	var total int
	listaF.Recorrer(func(fr Frecuencia) {
		total += (int)(fr.Frecuencia)
	})
	return total
}

/************************************************************************
*	Funciones basicas de una Lista, simplifican su manejo especifico			*
*************************************************************************/
func (listaF *ListaFrecuencia) Agregar(f Frecuencia) {
	*listaF = append(*listaF, f)
}

func (listaF *ListaFrecuencia) Recorrer(f func(Frecuencia)) {
	for i := 0; i < len(*listaF); i++ {
		frecuencia := (*listaF)[i]
		f(frecuencia)
	}
}

/*
	@func AportacionProductos sobre la tabla de productos seleccionados
	calculamos la suma total en pesos aportados
	@return regresamos la cantidad neta
*/
func (tablaFr *TablaFrecuencias) AportacionProductos() float64 {
	var aportacionTotal float64
	tablaFr.RecorrerFilas(func(fila ListaFrecuencia) {
		aportacionTotal += fila.CalcularAportacionVentas()
	})
	return aportacionTotal
}

func (tablaFr *TablaFrecuencias) TotalProductosVendidos() int {
	var totalProductos int
	tablaFr.RecorrerFilas(func(fila ListaFrecuencia) {
		totalProductos += fila.CalcularTotalProductos()
	})
	return totalProductos
}

/*
	@func TotalProductos
	Obtenemos la cantidad de productos dentro de la tabla
*/
func (tablaFr *TablaFrecuencias) TotalProductos() int {
	return len(tablaFr.Filas)
}

func (tablasFr *TablaFrecuencias) RecorrerFilas(f func(ListaFrecuencia)) {
	for i := 0; i < len(tablasFr.Filas); i++ {
		f(tablasFr.Filas[i])
	}
}

func (tablaFr *TablaFrecuencias) AgregarFila(fila *ListaFrecuencia) {
	tablaFr.Filas = append(tablaFr.Filas, *fila)
}

//Creamos los resultados para Frecuencia y promedio

func (tablaFr *TablaFrecuencias) ResultadosFrYProm(filtro *Filtro, params *Parametros) ListaResultadosFrProm {
	var listaResultados ListaResultadosFrProm
	var posicion, puntosIniciales int
	posicion = 1
	puntosIniciales = len(tablaFr.Filas)
	tablaFr.RecorrerFilas(func(fila ListaFrecuencia) {
		if fila.CalcularSumaVentas() > 0 {
			posicion++
			puntosIniciales--
			resultado := ResultadoFrecuenciaYPromedio{
				fila[0].IdProducto,
				float64(fila.CalcularFrecuenciaDiaria()),
				fila.CalcularPorcentajeFrecuenciaDiaria(filtro.ObtenerDias()),
				fila.CalcularPromedio(filtro.ObtenerDias()),
				"",
				0,
			}
			listaResultados = append(listaResultados, resultado)
		}
	})
	AsignarCategoriaYPuntosPFR(listaResultados, *params)
	return listaResultados
}

//Creamos los resultados para suma tiquets

func (tablaFr *TablaFrecuencias) ResultadosSumaTiquets(filtro *Filtro, params *Parametros) ListaResultadosSumaTiquets {
	var listaResultados ListaResultadosSumaTiquets
	var posicion, puntosIniciales int
	posicion = 1
	puntosIniciales = len(tablaFr.Filas)
	tablaFr.RecorrerFilas(func(fila ListaFrecuencia) {
		if fila.CalcularSumaVentas() > 0 {
			posicion++
			puntosIniciales--
			resultado := ResultadosSumaTiquets{
				fila[0].IdProducto,
				fila.CalcularSumaVentas(),
				fila.CalcularPorcentajeTiquetsVendidos(tablaFr.TotalProductosVendidos()),
				"",
				0,
			}
			listaResultados = append(listaResultados, resultado)
		}
	})
	AsignarCategoriaYPuntosST(listaResultados, *params)
	return listaResultados
}

//Creamos los resultados para suma saldos por producto

func (tablaFr *TablaFrecuencias) ResultadosSumaSaldos(filtro *Filtro, params *Parametros) ListaResultadosSumaSaldos {
	var listaResultados ListaResultadosSumaSaldos
	var posicion, puntosIniciales int
	posicion = 1
	puntosIniciales = len(tablaFr.Filas)
	tablaFr.RecorrerFilas(func(fila ListaFrecuencia) {
		if fila.CalcularSumaVentas() > 0 {
			posicion++
			puntosIniciales--
			resultado := ResultadosSumaSaldos{
				fila[0].IdProducto,
				fila.CalcularAportacionVentas(),
				fila.PorcentajeAportacionMarginal(tablaFr.AportacionProductos()),
				"",
				0,
			}
			listaResultados = append(listaResultados, resultado)
		}
	})
	AsignarCategoriaYPuntosSS(listaResultados, *params)
	return listaResultados
}

func (tablaFr *TablaFrecuencias) PuntuacionFinal(filtro Filtro, paramsABC Parametros, consideraciones []string) ListaResultadosFinales {
	var listaResultados ListaResultadosFinales
	if len(consideraciones) == 0 {
		return listaResultados
	}
	var listaFRP ListaResultadosFrProm
	var listaST ListaResultadosSumaTiquets
	var listaSV ListaResultadosSumaSaldos
	for i := 0; i < len(consideraciones); i++ {
		if strings.Compare(consideraciones[i], "FRP") == 0 {
			listaFRP = tablaFr.ResultadosFrYProm(&filtro, &paramsABC)
		}
		if strings.Compare(consideraciones[i], "ST") == 0 {
			listaST = tablaFr.ResultadosSumaTiquets(&filtro, &paramsABC)
		}
		if strings.Compare(consideraciones[i], "SV") == 0 {
			listaSV = tablaFr.ResultadosSumaSaldos(&filtro, &paramsABC)
		}
	}

	if len(listaFRP) == 0 && len(listaST) == 0 && len(listaSV) == 0 {
		return listaResultados
	}

	tablaFr.RecorrerFilas(func(fila ListaFrecuencia) {
		resultadoFinal := ResultadosFinales{
			PuntosFRP: 1,
			PuntosST:  1,
			PuntosSS:  1,
		}
		resultadoFinal.SKU = fila[0].IdProducto
		if len(listaFRP) > 0 {
			for i := 0; i < len(listaFRP); i++ {
				if strings.Compare(resultadoFinal.SKU, listaFRP[i].SKU) == 0 {
					resultadoFinal.PuntosFRP = float64(listaFRP[i].Puntos) * paramsABC.MultiplicadorFRP
					break
				}
			}
		}
		if len(listaST) > 0 {
			for i := 0; i < len(listaST); i++ {
				if strings.Compare(resultadoFinal.SKU, listaST[i].SKU) == 0 {
					resultadoFinal.PuntosST = float64(listaST[i].Puntos) * paramsABC.MultiplicadorST
					break
				}
			}
		}
		if len(listaSV) > 0 {
			for i := 0; i < len(listaSV); i++ {
				if strings.Compare(resultadoFinal.SKU, listaSV[i].SKU) == 0 {
					resultadoFinal.PuntosSS = float64(listaSV[i].Puntos) * paramsABC.MultiplicadorSV
					break
				}
			}
		}
		listaResultados = append(listaResultados, resultadoFinal)
	})
	//Calculamos los puntos de la tabla final para despues ordenarla por puntos
	for i := 0; i < len(listaResultados); i++ {
		listaResultados[i].Puntos = listaResultados[i].PuntosFRP * listaResultados[i].PuntosST * listaResultados[i].PuntosSS
	}
	listaResultados.Ordenar()

	//Con base en su posicion final calculamos la Categoria
	RangoA := (len(listaResultados) * paramsABC.PorcentajeA) / 100
	RangoB := (len(listaResultados) * paramsABC.PorcentajeB) / 100
	posicion := 1
	for i := 0; i < len(listaResultados); i++ {
		listaResultados[i].Posicion = posicion
		if posicion <= RangoA {
			listaResultados[i].Categoria = "A"
		} else if posicion <= RangoB {
			listaResultados[i].Categoria = "B"
		} else {
			listaResultados[i].Categoria = "C"
		}
		posicion++
	}
	return listaResultados
}

/*
	@func ObtenerResultados
	Obtenemos uns arreglo con los resultados
*/
func (tablaFr *TablaFrecuencias) ObtenerResultados(filtro *Filtro) ListaResultados {

	/*********************************************************************************/
	var filasResultados ListaResultados
	var c int
	tablaFr.RecorrerFilas(func(fila ListaFrecuencia) {
		c++
		resultado := Resultado{
			c,
			fila[0].IdProducto,
			fila.CalcularFrecuenciaDiaria(),
			fila.CalcularPorcentajeFrecuenciaDiaria(filtro.ObtenerDias()),
			fila.CalcularSumaVentas(),
			fila.CalcularPorcentajeTiquetsVendidos(tablaFr.TotalProductosVendidos()),
			fila.CalcularPromedio(filtro.ObtenerDias()),
			fila.CalcularAportacionVentas(),
			fila.PorcentajeAportacionMarginal(tablaFr.AportacionProductos()),
		}
		filasResultados = append(filasResultados, resultado)
	})
	return filasResultados
}
