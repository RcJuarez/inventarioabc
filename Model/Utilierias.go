package Model

/*
	@func ObtenerDias
	funcion de apoyo, permite calcular la diferencia en dias entre un rango de
	fechas (incluidos ya en el filtro)
*/
func (filtro *Filtro) ObtenerDias() int {
	fechaInicioSeg := filtro.FechaInicio.Unix()
	fechaFinalSeg := filtro.FechaFinal.Unix()
	SegundosTotales := fechaFinalSeg - fechaInicioSeg
	dias := (SegundosTotales / 86400) + 1
	return int(dias)
}
