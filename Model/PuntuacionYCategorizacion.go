package Model

import (
	"sort"
)

//Para frecuencias y promedio
func AsignarCategoriaYPuntosPFR(lista ListaResultadosFrProm, params Parametros) {
	lista.Ordenar()
	posicion := 1
	puntos := len(lista)
	RangoA := (len(lista) * params.PorcentajeA) / 100
	RangoB := (len(lista) * params.PorcentajeB) / 100
	for i := 0; i < len(lista); i++ {
		if posicion <= RangoA {
			lista[i].Categoria = "A"
			lista[i].Puntos = puntos * params.FactorA
		} else if posicion <= (RangoA + RangoB) {
			lista[i].Categoria = "B"
			lista[i].Puntos = puntos * params.FactorB
		} else {
			lista[i].Categoria = "C"
			lista[i].Puntos = puntos * params.FactorC
		}
		posicion++
		puntos--
	}
}

//Para suma tiquets
func AsignarCategoriaYPuntosST(lista ListaResultadosSumaTiquets, params Parametros) {
	lista.Ordenar()
	posicion := 1
	puntos := len(lista)
	RangoA := (len(lista) * params.PorcentajeA) / 100
	RangoB := (len(lista) * params.PorcentajeB) / 100
	for i := 0; i < len(lista); i++ {
		if posicion <= RangoA {
			lista[i].Categoria = "A"
			lista[i].Puntos = puntos * params.FactorA
		} else if posicion <= (RangoA + RangoB) {
			lista[i].Categoria = "B"
			lista[i].Puntos = puntos * params.FactorB
		} else {
			lista[i].Categoria = "C"
			lista[i].Puntos = puntos * params.FactorC
		}
		posicion++
		puntos--
	}
}

//Para suma saldos
func AsignarCategoriaYPuntosSS(lista ListaResultadosSumaSaldos, params Parametros) {
	lista.Ordenar()
	posicion := 1
	puntos := len(lista)
	RangoA := (len(lista) * params.PorcentajeA) / 100
	RangoB := (len(lista) * params.PorcentajeB) / 100
	for i := 0; i < len(lista); i++ {
		if posicion <= RangoA {
			lista[i].Categoria = "A"
			lista[i].Puntos = puntos * params.FactorA
		} else if posicion <= (RangoA + RangoB) {
			lista[i].Categoria = "B"
			lista[i].Puntos = puntos * params.FactorB
		} else {
			lista[i].Categoria = "C"
			lista[i].Puntos = puntos * params.FactorC
		}
		posicion++
		puntos--
	}
}

func (f ListaResultadosFrProm) Ordenar() {
	sort.Sort(f)
}

func (f ListaResultadosFrProm) Len() int {
	return len(f)
}

func (f ListaResultadosFrProm) Swap(i, j int) {
	f[i], f[j] = f[j], f[i]
}

func (f ListaResultadosFrProm) Less(i, j int) bool {
	if f[i].FrecuenciaPorcentaje > f[j].FrecuenciaPorcentaje {
		return true
	}
	if f[i].FrecuenciaPorcentaje == f[j].FrecuenciaPorcentaje {
		if f[i].PromedioVentas > f[j].PromedioVentas {
			return true
		}
	}
	return false
}

/***********************************************
	Para Tiquets por productos
************************************************/

type ResultadosSumaTiquets struct {
	SKU                   string
	SumaTiquets           float64
	SumaTiquetsPorcentaje float64
	Categoria             string
	Puntos                int
}

type ListaResultadosSumaTiquets []ResultadosSumaTiquets

func (l ListaResultadosSumaTiquets) Ordenar() {
	sort.Sort(l)
}

func (l ListaResultadosSumaTiquets) Len() int {
	return len(l)
}

func (l ListaResultadosSumaTiquets) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func (l ListaResultadosSumaTiquets) Less(i, j int) bool {
	if l[i].SumaTiquetsPorcentaje > l[j].SumaTiquetsPorcentaje {
		return true
	}
	return false
}

/*********************************
	Para saldo de Ventas
**********************************/

type ResultadosSumaSaldos struct {
	SKU                    string
	SumaAportacion         float64
	ApotacionMarginalPesos float64
	Categoria              string
	Puntos                 int
}

type ListaResultadosSumaSaldos []ResultadosSumaSaldos

func (l ListaResultadosSumaSaldos) Ordenar() {
	sort.Sort(l)
}

func (l ListaResultadosSumaSaldos) Len() int {
	return len(l)
}

func (l ListaResultadosSumaSaldos) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func (l ListaResultadosSumaSaldos) Less(i, j int) bool {
	if l[i].ApotacionMarginalPesos > l[j].ApotacionMarginalPesos {
		return true
	}
	return false
}

/*********************************
	Calculo de Puntuacion Final
**********************************/

type ResultadosFinales struct {
	Posicion  int
	SKU       string
	PuntosST  float64
	PuntosFRP float64
	PuntosSS  float64
	Puntos    float64
	Categoria string
}

type ListaResultadosFinales []ResultadosFinales

func (l ListaResultadosFinales) Ordenar() {
	sort.Sort(l)
}

func (l ListaResultadosFinales) Len() int {
	return len(l)
}

func (l ListaResultadosFinales) Swap(i, j int) {
	l[i], l[j] = l[j], l[i]
}

func (l ListaResultadosFinales) Less(i, j int) bool {
	if l[i].Puntos > l[j].Puntos {
		return true
	}
	return false
}
