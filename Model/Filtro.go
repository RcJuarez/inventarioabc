package Model

import (
	"time"
)

type Filtro struct {
	FechaInicio time.Time
	FechaFinal  time.Time
	Claves      []string
	IdProducto  string
}

/************************************************
	Estructuras del Algororitmo ABC
************************************************/
type Parametros struct {
	PorcentajeA      int     `json:"porcentajeA"`
	PorcentajeB      int     `json:"porcentajeB"`
	PorcentajeC      int     `json:"porcentajeC"`
	FactorA          int     `json:"factorA"`
	FactorB          int     `json:"factorB"`
	FactorC          int     `json:"factorC"`
	MultiplicadorFRP float64 `json:"multiplicadorFRP"`
	MultiplicadorST  float64 `json:"multiplicadorST"`
	MultiplicadorSV  float64 `json:"multiplicadorSV"`
}

/************************************************************************
																																				*
		@type Frecuencia define un conjunto de una fecha con 	la cantidad
		de ventas
																																				*
*************************************************************************/
type Frecuencia struct {
	IdProducto       string
	ClaveAlmacen     string
	Fecha            time.Time
	Frecuencia       float64
	SumaPrecioVentas float64
}

/************************************************************************
																																				*
		@type ListaFrecuncia
		Modelo de una lista de frecuencias en las cuales calculamos valores
		sobre varios productos en relacion con su frecuencia de ventas
																																				*
*************************************************************************/
type ListaFrecuencia []Frecuencia

/************************************************************************
																																				*
		@type struct TablaFrecuenciass
		es una representacion de un slice de Listas de Frecuencia que
		permite hacer	calculos mas generales sobre un varios productos y su
		frecuencia de ventas
																																				*
*************************************************************************/
type TablaFrecuencias struct {
	Filas []ListaFrecuencia
}

type Resultado struct {
	NumFila                int
	SKU                    string
	Frecuencia             int
	FrecuenciaPorcentaje   float64
	SumaTiquets            float64
	SumaTiquetsPorcentaje  float64
	PromedioVentas         float64
	SumaAportacion         float64
	ApotacionMarginalPesos float64
}

type ListaResultados []Resultado

/************************************************
	Para Frecuencia y Promedio
*************************************************/

type ResultadoFrecuenciaYPromedio struct {
	SKU                  string
	Frecuencia           float64
	FrecuenciaPorcentaje float64
	PromedioVentas       float64
	Categoria            string
	Puntos               int
}

type ListaResultadosFrProm []ResultadoFrecuenciaYPromedio
