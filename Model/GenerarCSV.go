package Model

import (
	"fmt"
	"strings"
)

func GenerarCSV(l1 ListaResultados, l2 ListaResultadosFinales, params Parametros, consideraciones []string, filtro Filtro) string {
	datosAEscribir := fmt.Sprint(";FechaInicio;FechaFinal;\n")
	datosAEscribir += fmt.Sprintf(";%s;%s;\n", filtro.FechaInicio.Format("2006-02-01"), filtro.FechaFinal.Format("2006-02-01"))
	datosAEscribir += fmt.Sprintf(";Consideraciones del Algoritmo\n")
	for i := 0; i < len(consideraciones); i++ {
		if strings.Compare(consideraciones[i], "FRP") == 0 {
			datosAEscribir += ";Frecuencia y Promedio;\n"
		}
		if strings.Compare(consideraciones[i], "ST") == 0 {
			datosAEscribir += ";Suma de Tiquets\n"
		}
		if strings.Compare(consideraciones[i], "SV") == 0 {
			datosAEscribir += ";Suma de las aportaciones\n"
		}
	}
	datosAEscribir += fmt.Sprint(";Porcentaje de A; Porcentaje de B; Porcentaje de C;\n")
	datosAEscribir += fmt.Sprintf(";%d;%d;%d;\n", params.PorcentajeA, params.PorcentajeB, params.PorcentajeC)
	datosAEscribir += fmt.Sprint(";Factor de A; Factor de B; Factor de C;\n")
	datosAEscribir += fmt.Sprintf(";%d;%d;%d;\n", params.FactorA, params.FactorB, params.FactorC)
	datosAEscribir += fmt.Sprint(";Multiplicador de Frecuencia y promedio de ventas; Multiplicador de tiquets por producto ; Multiplicador de Aportacion por productos;\n")
	datosAEscribir += fmt.Sprintf(";%f;%f;%f\n\n\n", params.MultiplicadorFRP, params.MultiplicadorST, params.MultiplicadorSV)

	datosAEscribir += fmt.Sprint("Posicion;SKU;Frecuencia;FrecuenciaPorcentaje;SumaTiquets;SumaTiquetsPorcentaje;PromedioVentas;SumaAportacion;ApotacionMarginalPesos;PuntosFrecuenciaPromedio;PuntosSumaTiquets;PuntosSumaVentas;PuntosTotales;Categoria\n")
	for i := 0; i < len(l2); i++ {
		for j := 0; j < len(l1); j++ {
			if strings.Compare(l1[j].SKU, l2[i].SKU) == 0 {
				datosAEscribir += fmt.Sprintf(
					"%d;%s;%d;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%s\n",
					l2[i].Posicion,
					l2[i].SKU,
					l1[j].Frecuencia,
					l1[j].FrecuenciaPorcentaje,
					l1[j].SumaTiquets,
					l1[j].SumaTiquetsPorcentaje,
					l1[j].PromedioVentas,
					l1[j].SumaAportacion,
					l1[j].ApotacionMarginalPesos,
					l2[i].PuntosFRP,
					l2[i].PuntosST,
					l2[i].PuntosSS,
					l2[i].Puntos,
					l2[i].Categoria,
				)
				break
			}
		}
	}
	return datosAEscribir
}
