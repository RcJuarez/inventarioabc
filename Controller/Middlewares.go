package Controller

import (
	"log"

	"../Model"
	"../motoresDB/factory"
	"github.com/kataras/iris/context"
)

func Layout(ctx context.Context) {
	ctx.ViewLayout("layout/main.html")
	ctx.Next()
}

func PreLogin(ctx context.Context) {
	ctx.ViewLayout("layout/loginLayout.html")
	ctx.Next()
}

//funcion cargarSesionDefault

func C_abc_cargarFiltroInicial(ctx context.Context) {
	//Primero comprobamos que la sesion sea nueva
	if ctx.Session().GetString("Iniciada") == "" {
		//Si es nueva Asignamos valores default a los parametros
		log.Println("Sesion iniciada...")
		filtro := Model.Filtro{}
		consultasPg := factory.Consultas("postgres")
		err := consultasPg.ObtenerRangoFechas(&filtro)
		logErrores(err)
		listaAlmacenes, err := consultasPg.ObtenerAlmacenes(&filtro)
		logErrores(err)
		parametros := Model.Parametros{
			PorcentajeA:      20,
			PorcentajeB:      30,
			PorcentajeC:      50,
			FactorA:          3,
			FactorB:          2,
			FactorC:          1,
			MultiplicadorFRP: 1.000,
			MultiplicadorST:  1.000,
			MultiplicadorSV:  1.000,
		}
		consideraciones := []string{"FRP", "ST", "SV"}
		ctx.Session().Set("consideraciones", consideraciones)
		ctx.Session().Set("filtro", filtro)
		ctx.Session().Set("fechaMin", filtro.FechaInicio.Format("2006-01-02"))
		ctx.Session().Set("fechaMax", filtro.FechaFinal.Format("2006-01-02"))
		ctx.Session().Set("listaAlmacenes", listaAlmacenes)
		ctx.Session().Set("parametros", parametros)
		ctx.Session().Set("Iniciada", "Ok")
	} else {
		log.Println("sesion activa")
	}
	ctx.Next()
}
