package Controller

import (
	"encoding/json"
	"strconv"
	"strings"
	"time"

	"../Model"
	"../motoresDB/factory"
	"github.com/kataras/iris/context"
)

func ModificarFecha(ctx context.Context) {
	filtro := ObtenerFiltroDeSesion(ctx)
	type request struct {
		FechaInicio string `json:"fechaInicio"`
		FechaFinal  string `json:"fechaFinal"`
	}
	var peticion request
	datos := ctx.FormValue("fechas")
	err := json.Unmarshal([]byte(datos), &peticion)
	logErrores(err)
	fi, err := time.Parse("2006-01-02", peticion.FechaInicio[1:11])
	ff, err := time.Parse("2006-01-02", peticion.FechaFinal[1:11])
	logErrores(err)
	filtro.FechaInicio = fi
	filtro.FechaFinal = ff
	ctx.Session().Set("filtro", filtro)
	ctx.Write([]byte("ok"))
}

func ObtenerFechasPorAlmacenes(ctx context.Context) {
	consultasPg := factory.Consultas("postgres") //Recuperamos los datos de la Fuente de datos (Postgres)
	filtro := ObtenerFiltroDeSesion(ctx)
	var listaAlmacenes []string
	err := json.Unmarshal([]byte(ctx.FormValue("listaAlmacenes")), &listaAlmacenes)
	logErrores(err)
	filtro.Claves = listaAlmacenes
	filtroAux := filtro
	err = consultasPg.ObtenerRangoFechas(&filtroAux)
	logErrores(err)
	type respuesta struct {
		FechaInicio string `json:"fechaInicio"`
		FechaFinal  string `json:"fechaFinal"`
	}
	r := respuesta{
		FechaInicio: strings.Split(filtroAux.FechaInicio.String(), " ")[0],
		FechaFinal:  strings.Split(filtroAux.FechaFinal.String(), " ")[0],
	}
	res, err := json.Marshal(r)
	logErrores(err)
	ctx.Session().Set("fechaMin", filtroAux.FechaInicio.Format("2006-01-02"))
	ctx.Session().Set("fechaMax", filtroAux.FechaFinal.Format("2006-01-02"))
	ctx.Session().Set("filtro", filtro)
	ctx.Write(res)
}

func ConfigurarParametrosABC(ctx context.Context) {
	paramsABC := ctx.Session().Get("parametros").(Model.Parametros)
	err := json.Unmarshal([]byte(ctx.FormValue("parametrosABC")), &paramsABC)
	logErrores(err)
	ctx.Session().Set("parametros", paramsABC)
	ctx.Write([]byte("ok"))
}

func ConsideracionesAlgoritmoABC(ctx context.Context) {
	consideraciones := []string{}
	err := json.Unmarshal([]byte(ctx.FormValue("consideraciones")), &consideraciones)
	logErrores(err)
	ctx.Session().Set("consideraciones", consideraciones)
	ctx.Write([]byte("Ok"))
}

func EnviarCSV(ctx context.Context) {
	consultasPg := factory.Consultas("postgres")         //Recuperamos los datos de la Fuente de datos (Postgres)
	filtro := ctx.Session().Get("filtro").(Model.Filtro) //Obtenemos el filtro de la sesion
	consideraciones := ctx.Session().Get("consideraciones").([]string)
	paramsAlgoritmo := ctx.Session().Get("parametros").(Model.Parametros)

	datosTabla, err := consultasPg.DatosFrecuencias(&filtro)
	logErrores(err)
	Resultados1 := datosTabla.ObtenerResultados(&filtro)
	Resultados2 := datosTabla.PuntuacionFinal(filtro, paramsAlgoritmo, consideraciones)

	datosCSV := Model.GenerarCSV(Resultados1, Resultados2, paramsAlgoritmo, consideraciones, filtro)
	ctx.Header("Content-type", "application/octet-stream")
	ctx.Header("Content-Length", strconv.Itoa(len(datosCSV)))
	ctx.Header("Content-disposition", "attachment;filename=ClasificacionABC-"+time.Now().Format("2016-01-02")+".csv")
	ctx.Write([]byte(datosCSV))
}
