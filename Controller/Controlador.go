package Controller

import (
	"log"
	"strconv"

	"../Model"
	"../View"
	"../View/general"
	"../motoresDB/factory"
	"github.com/kataras/iris/context"
)

func logErrores(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func ObtenerFiltroDeSesion(ctx context.Context) Model.Filtro {
	return ctx.Session().Get("filtro").(Model.Filtro) //Obtenemos el filtro de la sesion
}

func ObtenerParametrosDeSession(ctx context.Context) Model.Parametros {
	return ctx.Session().Get("parametros").(Model.Parametros)
}

/*
	Funciones llamadas desde el Servidor (Server.go)
	para las rutas del sitio web
*/

func Dashboard(ctx context.Context) {
	err := ctx.View("Dashboard.html")
	if err != nil {
		log.Fatalln(err)
	}
}

func Login(ctx context.Context) {
	ctx.View("Login.html")
}

func C_abc_cargarPanel(ctx context.Context) {
	/*********************
	  Inicializar Variables
	*********************/
	type Contenido struct { //Definimos una estructura que permite definir al contenido especifico para este metodo
		TablaResultados Model.ListaResultadosFinales
		ParametrosABC   Model.Parametros
		Fechas          View.Fechas
		Almacenes       View.ListaAlmacen
		Consideraciones View.Consideraciones
	}
	var contenido Contenido
	consultasPg := factory.Consultas("postgres")         //Creamos una variable del tipo contenido                       //Recuperamos los datos de la Fuente de datos (Postgres)
	filtro := ctx.Session().Get("filtro").(Model.Filtro) //Obtenemos el filtro de la sesion
	consideraciones := ctx.Session().Get("consideraciones").([]string)
	fMax := ctx.Session().GetString("fechaMax")
	fMin := ctx.Session().GetString("fechaMin")
	paramsAlgoritmo := ctx.Session().Get("parametros").(Model.Parametros) //Parametros del usuario para el algoritmo Clasificador ABC
	var ContenidoPagina general.Pagina                                    //Creamos la variable pagina que identifica el contexto de este Render
	var mensaje general.Mensaje                                           //Dentro de pagina existe Mensaje
	var paginacion general.Paginacion                                     // Dentro de Pagina existe Paginacion

	pagina, err := strconv.Atoi(ctx.URLParam("pagina")) //Obtenemos el parametro dentro de la URL
	logErrores(err)
	logErrores(err)
	listaAlmacenes := ctx.Session().Get("listaAlmacenes").([][2]string)

	/**************************************************
		Buscamos los datos pedidos en la fuente de datos
	***************************************************/
	datosTabla, err := consultasPg.DatosFrecuencias(&filtro)
	logErrores(err)
	//Resultados := datosTabla.ObtenerResultados(&filtro) //Creamos un slice de los resultados

	Resultados := datosTabla.PuntuacionFinal(filtro, paramsAlgoritmo, consideraciones)
	/*******	Preparamos la vista		******/
	contenido = Contenido{
		Consideraciones: View.CrearConsideraciones(consideraciones),
		ParametrosABC:   paramsAlgoritmo,
		Fechas:          View.CrearFechas(filtro),
		Almacenes:       View.CrearLista(listaAlmacenes, filtro.Claves),
	}
	contenido.Fechas.FechaMin = fMin
	contenido.Fechas.FechaMax = fMax
	if len(Resultados) == 0 {
		//Arrojamos un Mensaje
		mensaje = general.Mensaje{
			Tiene:   true,
			Mensaje: "No existen datos para esos almacenes en las fechas solicitadas ",
			Tipo:    "info",
		}
	} else if len(Resultados) > 20 {
		//Creamos la paginacion
		paginacion = general.Paginacion{
			Tiene:          true,
			ValorPrevio:    pagina - 1,
			ValorSiguiente: pagina + 1,
		}
		if pagina > 1 {
			paginacion.TienePrevio = true
		}
		paginaMax := 0
		if pagina%20 == 0 {
			paginaMax = len(Resultados) / 20
		} else {
			paginaMax = int(len(Resultados)/20) + 1
		}
		filasPorPagina := pagina * 20
		if pagina < paginaMax {
			paginacion.TieneSiguiente = true
			contenido.TablaResultados = Resultados[filasPorPagina-20 : filasPorPagina]
		} else {
			contenido.TablaResultados = Resultados[filasPorPagina-20:]
		}
		ContenidoPagina.Paginacion = paginacion
	} else {
		contenido.TablaResultados = Resultados
	}
	ContenidoPagina.Mensaje = mensaje
	ContenidoPagina.Contenido = contenido
	ctx.ViewData("ContenidoPagina", ContenidoPagina)
	err = ctx.View("panel.html")
	logErrores(err)
	ctx.Session().Set("filtro", filtro)
}

func C_abc_config(ctx context.Context) {
	/*********************
	  Inicializar Variables
	*********************/
	paramsAlgoritmo := ctx.Session().Get("parametros").(Model.Parametros) //Parametros del usuario para el algoritmo Clasificador ABC
	ctx.ViewData("parametros", paramsAlgoritmo)
	err := ctx.View("config.html")
	if err != nil {
		log.Fatalln(err)
	}
}
