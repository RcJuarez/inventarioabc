package main

import (
	"ProyectoClasificadorABC/Model"
	"ProyectoClasificadorABC/motoresDB/factory"
	"bufio"
	"fmt"
	"os"
)

func main() {
	testObtenerSkuProductos()
	//testRangoFechas()
	//testDatosFrecuncias()
	//testTablaFrecuencias()
}

func testObtenerSkuProductos() {
	fmt.Println("****************************************************")
	fmt.Println("Obtener lista de todos los sku's en la base")
	filtro := Model.Filtro{
		IdAlmacen: "D0",
	}
	consultasPg := factory.Consultas("postgres")
	listarSkus, err := consultasPg.ObtenerSkus(&filtro)
	if err != nil {
		fmt.Println(err)
	}
	for i := 0; i < len(listarSkus); i++ {
		fmt.Println(listarSkus[i])
	}
	fmt.Println(len(listarSkus), filtro)
}

func testRangoFechas() {
	fmt.Println("****************************************************")
	fmt.Println("Obtener fecha max y min segun filtro")
	filtro := Model.Filtro{}
	consultasPg := factory.Consultas("postgres")
	err := consultasPg.ObtenerRangoFechas(&filtro)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(filtro)
}

func testDatosFrecuncias() {
	fmt.Println("****************************************************")
	fmt.Println("generar tabal de frecuencias")
	filtro := Model.Filtro{}
	consultasPg := factory.Consultas("postgres")
	datosTabla, err := consultasPg.DatosFrecuencias(&filtro)
	if err != nil {
		fmt.Println(err)
	}
	c := 0
	datosTabla.RecorrerFilas(func(fila Model.ListaFrecuencia) {
		c++
		fmt.Println("Fila -", c)
		fila.Recorrer(func(fr Model.Frecuencia) {
			fmt.Println("----", fr)
		})
	})
}

func testTablaFrecuencias() {
	fmt.Println("****************************************************")
	fmt.Println("obtenemos datos para tabular")
	file, err := os.Create("./Datos.csv")
	if err != nil {
		fmt.Println(err)
	}
	w := bufio.NewWriter(file)
	filtro := Model.Filtro{
		IdAlmacen: "D0",
	}
	consultasPg := factory.Consultas("postgres")
	err = consultasPg.ObtenerRangoFechas(&filtro)
	datosTabla, err := consultasPg.DatosFrecuencias(&filtro)
	if err != nil {
		fmt.Println(err)
	}
	c := 0
	fmt.Printf("%2d | %14s | %14s | %15s | %15s | %15s \n", "num", "Frecuencia", "porcentajeFr", "SumaTiquet", "pocentajeTq", "Promedio")
	datosTabla.RecorrerFilas(func(fila Model.ListaFrecuencia) {
		c++
		fmt.Printf("%2d | %14d | %14f | %15f | %15f | %15f \n",
			c,
			fila.CalcularFrecuenciaDiaria(),
			fila.CalcularPorcentajeFrecuenciaDiaria(filtro.ObtenerDias()),
			fila.CalcularSumaVentas(),
			fila.CalcularPorcentajeTiquetsVendidos(len(datosTabla.Filas)),
			fila.CalcularPromedio(filtro.ObtenerDias()),
		)
		datosAEscribirArchivo := fmt.Sprintf(
			"%d|%f|%f|%f|%f\n",
			fila.CalcularFrecuenciaDiaria(),
			fila.CalcularPorcentajeFrecuenciaDiaria(filtro.ObtenerDias()),
			fila.CalcularSumaVentas(),
			fila.CalcularPorcentajeTiquetsVendidos(len(datosTabla.Filas)),
			fila.CalcularPromedio(filtro.ObtenerDias()),
		)
		n, err := w.WriteString(datosAEscribirArchivo)
		if err != nil {
			fmt.Println(err)
		}
		fmt.Println("bytes escritos : ", n)
	})
	fmt.Println(filtro, "  ", len(datosTabla.Filas))
	w.Flush()
}
