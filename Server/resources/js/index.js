
//Diseño Web
//login end
$(function(){var textfield = $("input[name=user]");
$('button[type="submit"]').click(function(e) {
e.preventDefault();
//little validation just to check username
if (textfield.val() != "") {
//$("body").scrollTo("#output");
$("#output").addClass("alert alert-success animated fadeInUp").html("Bienvenido " + "<span style='text-transform:uppercase'>" + textfield.val() + "</span>");
$("#output").removeClass(' alert-danger');
$("input").css({
"height":"0",
"padding":"0",
"margin":"0",
"opacity":"0"
});
//change button text 
$('button[type="submit"]').html("continuar")
.removeClass("btn-info")
.addClass("btn-default").click(function(){
$("input").css({
"height":"auto",
"padding":"10px",
"opacity":"1"
}).val("");
window.location = "/ClasificadorABC"
});

} else {
//remove success mesage replaced with error message
$("#output").removeClass(' alert alert-success');
$("#output").addClass("alert alert-danger animated fadeInUp").html("sorry enter a username ");
}
});
});

//Login end

//PopUp

//PopUp END
//Diseño Web END

//Peticiones AJAX
//Registra almacenes selccionados

function seleccionaAlmacenes() {
	var almacenes = document.getElementsByClassName("almacen")
	var listaAlmacenes = []
	for (var i = almacenes.length - 1; i >= 0; i--) {
		if(almacenes[i].checked){
			if(listaAlmacenes.indexOf(almacenes[i]) != undefined){
				listaAlmacenes.push(almacenes[i].value)
			}
		}
	}
	console.log(listaAlmacenes)
	return listaAlmacenes
}

var clickAlmacenes = function() {
	$.ajax(
	{
		url: "http://localhost:8080/API/Almacenes",
		type: "POST",
		data: {
			listaAlmacenes: JSON.stringify(seleccionaAlmacenes())
		}
	})
	.done(function(res) {
		data = JSON.parse(res)
		var fechaInicio = document.getElementById("fechaInicio")
		var fechaFinal = document.getElementById("fechaFinal")
		var fmin = document.getElementById("fechaMin")
		var fmax = document.getElementById("fechaMax")
		fmin.textContent = data.fechaInicio
		fmax.textContent = data.fechaMax
		fechaInicio.setAttribute("min", data.fechaInicio)
		fechaInicio.setAttribute("max", data.fechaFinal)
		fechaInicio.setAttribute("value", data.fechaInicio)
		fechaFinal.setAttribute("min", data.fechaInicio)
		fechaFinal.setAttribute("max", data.fechaFinal)
		fechaFinal.setAttribute("value", data.fechaFinal)
	})
}

var fechaCambia = function fechaCambia(ev) {
	var fechaInicio = document.getElementById("fechaInicio")
	var fechaFinal = document.getElementById("fechaFinal")
	fi = new Date(fechaInicio.value)
	ff = new Date(fechaFinal.value)
	if (fi.getTime() > ff.getTime()){
		fechaInicio.value = fechaInicio.min
		fechaFinal.value = fechaFinal.max
    var mensajeDiv = document.getElementById("mensajeDiv")
		var mesajeTitulo = document.getElementById("mensajeTitulo")
		var mensaje = document.getElementById("mensaje")
		mensajeDiv.style.opacity = 1
		mensajeDiv.className = "alert alert-warning"
		mensaje.textContent = "Cuidado"
		mesajeTitulo.textContent = "Las fecha inicial no puede ser mayor que la final"
		setTimeout(function() {
			mensajeDiv.style.opacity = 0
		}, 4000)
		return
	}
	$.ajax(
	{
		url: "http://localhost:8080/API/CambiarFecha",
		type: "POST",
		data: {
			fechas: JSON.stringify({
				fechaInicio: JSON.stringify(fechaInicio.value),
				fechaFinal: JSON.stringify(fechaFinal.value)
			})
		}
	})
	.done(function(data){
		console.log(data)
	})
}
document.getElementById("fechaInicio").onchange = fechaCambia
document.getElementById("fechaFinal").onchange = fechaCambia

function seleccionaConsideraciones() {
	var cons = document.getElementsByClassName("consideraciones")
	var listaCons = []
	for (var i = cons.length - 1; i >= 0; i--) {
		if(cons[i].checked){
			listaCons.push(cons[i].value)
		}
	}
	console.log(listaCons)
	return listaCons
}

function cambiarConsideraciones() {
	$.ajax(
	{
		url:"http://localhost:8080/API/consideraciones",
		type: "POST",
		data: {
			consideraciones: JSON.stringify(seleccionaConsideraciones())
		}
	})
	.done(function(data) {
		console.log(data)
	})
}

//Peticiones AJAX END
	
//

var previaConfig = document.getElementById("previaConfig")
var previaMinimizado = false
previaConfig.addEventListener("click", function (ev) {
	if (!previaMinimizado) {
		ev.target.style.top = "90%"
	}
	else{
		ev.target.style.top = "45%"
	}
	previaMinimizado = !previaMinimizado
}, true)