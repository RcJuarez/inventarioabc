function sumarABC(){
	suma = app.ParametrosABC.porcentajeA +
	app.ParametrosABC.porcentajeB +
	app.ParametrosABC.porcentajeC
	return suma
}

function modificarPorcentajeA(valor) {
	if (valor < 0){
		return
	}
	suma = app.ParametrosABC.porcentajeB +
	app.ParametrosABC.porcentajeC +
	valor
	if (suma <= 100) {
		app.ParametrosABC.porcentajeA = valor
		pA = document.getElementById("porcentajeA")
		pA.textContent = valor+"%"
	}
}

function modificarPorcentajeB(valor) {
	if (valor < 0){
		return
	}
	suma = app.ParametrosABC.porcentajeA +
	app.ParametrosABC.porcentajeC +
	valor
	if (suma <= 100) {
		app.ParametrosABC.porcentajeB = valor
		pB = document.getElementById("porcentajeB")
		pB.textContent = valor+"%"
	}
}

function modificarPorcentajeC(valor) {
	if (valor < 0){
		return
	}
	suma = app.ParametrosABC.porcentajeA +
	app.ParametrosABC.porcentajeB +
	valor
	if (suma <= 100) {
		app.ParametrosABC.porcentajeC = valor
		pC = document.getElementById("porcentajeC")
		pC.textContent = valor+"%"
	}
}

function igualA100(){
	var popup = document.getElementById("popupPorcentajesABC")
	if (sumarABC() < 100){
		var mensajeDiv = document.getElementById("mensajeDiv")
		var mesajeTitulo = document.getElementById("mensajeTitulo")
		var mensaje = document.getElementById("mensaje")
		mensajeDiv.style.opacity = 1
		mensajeDiv.className = "alert alert-warning"
		mensaje.textContent = "Cuidado"
		mesajeTitulo.textContent = "La suma ABC debe ser igual a 100%"
		setTimeout(function() {
			mensajeDiv.style.opacity = 0
		}, 4000)

		document.getElementById("btn-enviar").disabled = true
	}
	if (sumarABC() == 100){
		document.getElementById("btn-enviar").disabled = false
	}
}

document.getElementById("restarA").onclick = function(){
	var numSigueinte = app.ParametrosABC.porcentajeA - 1
	modificarPorcentajeA(numSigueinte)
	igualA100()
}

document.getElementById("sumarA").onclick = function(){
	var numSigueinte = app.ParametrosABC.porcentajeA + 1
	modificarPorcentajeA(numSigueinte)
	igualA100()
}

document.getElementById("restarB").onclick = function(){
	var numSigueinte = app.ParametrosABC.porcentajeB - 1
	modificarPorcentajeB(numSigueinte)
	igualA100()
}

document.getElementById("sumarB").onclick = function(){
	var numSigueinte = app.ParametrosABC.porcentajeB + 1
	modificarPorcentajeB(numSigueinte)
	igualA100()
}

document.getElementById("restarC").onclick = function(){
	var numSigueinte = app.ParametrosABC.porcentajeC - 1
	modificarPorcentajeC(numSigueinte)
	igualA100()
}

document.getElementById("sumarC").onclick = function(){
	var numSigueinte = app.ParametrosABC.porcentajeC + 1
	modificarPorcentajeC(numSigueinte)
	igualA100()
}

document.getElementById("factorA").onclick = function() {
	app.ParametrosABC.factorA = parseInt(document.getElementById("factorA").children[1].value)
	console.log(app)
}

document.getElementById("factorB").onclick = function() {
	app.ParametrosABC.factorB = parseInt(document.getElementById("factorB").children[1].value)
	console.log(app)
}

document.getElementById("factorC").onclick = function() {
	app.ParametrosABC.factorC = parseInt(document.getElementById("factorC").children[1].value)
	console.log(app)
}

document.getElementById("multiplicadorFRP").onkeyup = document.getElementById("multiplicadorFRP").onchange = function(ev) {
	var mfrp = ev.target
	RE = /([0-9]+(\.[0-9]{1,3}))|([0-9]+)/
	verificar = RE.exec(mfrp.value)
	if (verificar != null){
		mfrp.value = verificar[0]
		app.ParametrosABC.multiplicadorFRP = parseFloat(mfrp.value)
	}
	console.log(app)
}
document.getElementById("multiplicadorST").onkeyup = document.getElementById("multiplicadorST").onchange = function(ev) {
	var mst = ev.target
	RE = /([0-9]+(\.[0-9]{1,3}))|([0-9]+)/
	verificar = RE.exec(mst.value)
	if (verificar != null){
		mst.value = verificar[0]
		app.ParametrosABC.multiplicadorST = parseFloat(mst.value)
	}
	console.log(app)
}
document.getElementById("multiplicadorSV").onkeyup = document.getElementById("multiplicadorSV").onchange = function(ev) {
	var msv = ev.target
	RE = /([0-9]+(\.[0-9]{1,3}))|([0-9]+)/
	verificar = RE.exec(msv.value)
	if (verificar != null){
		msv.value = verificar[0]
		app.ParametrosABC.multiplicadorST = parseFloat(msv.value)
	}
	console.log(app)
}

/**************************************************************
						Peticionaes AJAX
**************************************************************/
function modificarParametrosABC(){
	$.ajax(
	{
		url:"http://localhost:8080/API/configParametrosABC",
		type: "Post",
		data: {
			parametrosABC: JSON.stringify(app.ParametrosABC)
		}
	})
	.done(function(data){
		var mensajeDiv = document.getElementById("mensajeDiv")
		var mesajeTitulo = document.getElementById("mensajeTitulo")
		var mensaje = document.getElementById("mensaje")
		mensajeDiv.className = "alert alert-info"
		mensajeDiv.style.opacity = 1
		mensaje.textContent = "Notificacion"
		mesajeTitulo.textContent = "Se han Guardado los parametros"
		setTimeout(function() {
			mensajeDiv.style.opacity = 0
		}, 3000)
		console.log("parametrosCambiados")
	})
}
document.getElementById("btn-enviar").onclick = modificarParametrosABC

/**************************************************************
						Javascript para estilos de componentes
**************************************************************/
$(function() {
    var action;
    $(".number-spinner button").mousedown(function () {
        btn = $(this);
        input = btn.closest('.number-spinner').find('input');
        btn.closest('.number-spinner').find('button').prop("disabled", false);

    	if (btn.attr('data-dir') == 'up') {
            action = setInterval(function(){
                if ( input.attr('max') == undefined || parseInt(input.val()) < parseInt(input.attr('max')) ) {
                    input.val(parseInt(input.val())+1);
                }else{
                    btn.prop("disabled", true);
                    clearInterval(action);
                }
            }, 50);
    	} else {
            action = setInterval(function(){
                if ( input.attr('min') == undefined || parseInt(input.val()) > parseInt(input.attr('min')) ) {
                    input.val(parseInt(input.val())-1);
                }else{
                    btn.prop("disabled", true);
                    clearInterval(action);
                }
            }, 50);
    	}
    }).mouseup(function(){
        clearInterval(action);
    });
});

