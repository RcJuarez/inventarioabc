package main

import (
	"strconv"

	"../Controller"
	"github.com/kataras/iris"
	"github.com/kataras/iris/sessions"
	"github.com/kataras/iris/view"
)

var sessionId int

func main() {
	//Creamos una aplicacion web con kataras/iris
	app := iris.New()
	//Asignamos un identificador a cada sesion y la ligamos a la aplicacion
	sessionId++
	sesion := sessions.New(sessions.Config{Cookie: "sesion" + strconv.Itoa(sessionId)})
	app.AttachSessionManager(sesion)
	app.AttachView(view.HTML("../View/Templates", ".html").Reload(true)) //le decimos a la aplicacion donde se encuentran los recursos .HTML .CSS .JS
	app.StaticWeb("/static", "./resources")

	/*
		Routeo de la App
		Le indicamos que hara para cada URL de la peticion
	*/

	clasificadorABC := app.Party(
		"/ClasificadorABC",
		Controller.Layout,
		Controller.C_abc_cargarFiltroInicial,
	)

	clasificadorABC.Get("/",
		Controller.C_abc_cargarPanel)

	clasificadorABC.Get(
		"/config",
		Controller.C_abc_config,
	)

	app.Get("/Dashboard", Controller.Layout, Controller.Dashboard)

	login := app.Party("/Login")
	login.Get("/",
		Controller.PreLogin,
		Controller.Login,
	)

	API := app.Party("/API")
	API.Post(
		"/Almacenes",
		Controller.ObtenerFechasPorAlmacenes,
	)
	API.Post(
		"/CambiarFecha",
		Controller.ModificarFecha,
	)
	API.Post(
		"/configParametrosABC",
		Controller.ConfigurarParametrosABC,
	)
	API.Post(
		"/consideraciones",
		Controller.ConsideracionesAlgoritmoABC,
	)
	API.Get(
		"/crearCSV",
		Controller.EnviarCSV,
	)
	app.Run(iris.Addr(":8080"))
}
