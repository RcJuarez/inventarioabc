﻿create table "datos_kore".Almacen(
idAlmacen varchar(20) primary key,
nombre varchar(2) not null
);

create table "datos_kore".Producto(
idProducto varchar(20) primary key,
descripcion varchar(50) not null,
lineaDept varchar(50) not null,
marca varchar(20) not null
);

create table "datos_kore".Tiquet (
idTiquet varchar(20) primary key,
fechaRegistro date not null,
idAlmacen bigserial not null,
foreign key (idAlmacen) references "datos_kore".Almacen(idAlmacen)
);

create table "datos_kore".DetalleTiquet (
idDetalleTiquet varchar(20) primary key,
idTiquet varchar(20),
idProducto varchar(20),
foreign key (idTiquet) references "datos_kore".Tiquet(idTiquet),
foreign key (idProducto) references "datos_kore".Producto(idProducto)
);

--drop table "DatosKore".Almacen
--drop table "DatosKore".Producto