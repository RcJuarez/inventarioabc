package interfaces

import (
	"../../Model"
)

type IFrecuencia interface {
	DatosFrecuencias(filtro *Model.Filtro) (*Model.TablaFrecuencias, error)
	ObtenerRangoFechas(filtro *Model.Filtro) error
	ObtenerSkus(*Model.Filtro) ([]string, error)
	ObtenerClaveAlmacenes(*Model.Filtro) ([]string, error)
	ObtenerAlmacenes(filtro *Model.Filtro) ([][2]string, error)
}
