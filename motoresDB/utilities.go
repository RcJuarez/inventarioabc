package motoresDB

import (
	"encoding/json"
	"os"
)

func GetConfiguration() (Config, error) {
	config := Config{}

	file, err := os.Open("../config.json")
	if err != nil {
		return config, err
	}

	defer file.Close()
	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)

	if err != nil {
		return config, err
	}
	return config, err
}
