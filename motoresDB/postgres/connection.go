package postgres

import (
	"database/sql"
	"fmt"
	"log"

	"../../motoresDB"
	_ "github.com/lib/pq"
)

/*
	Get() establece la conexion al motor de PotgresSQL
*/

func Get() *sql.DB {
	config, err := motoresDB.GetConfiguration()
	if err != nil {
		log.Fatal(err)
	}
	/*
		URL de conexion a Postgres:
		DSN => postgres:user:password@server:port/databasename?sslmode=false
		Con la libreria sql/db se crea la conexion
	*/
	dsn := fmt.Sprintf("%s://%s:%s@%s:%s/%s?sslmode=%s", config.Engine, config.User, config.Password, config.Server, config.Port, config.Dbname, config.SSLmode)
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		log.Fatal(err)
	}

	/*
		Comprobamos la conexion realizando un ping a la base de datos
	*/
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}
	//Retornamos la conexion a Porstgres
	return db
}
