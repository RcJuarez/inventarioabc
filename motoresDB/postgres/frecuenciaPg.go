package postgres

import (
	"strings"
	"time"

	"../../Model"
)

type Consultas struct{}

func WhereGenerico(filtro *Model.Filtro) string {
	var where string
	if !filtro.FechaInicio.IsZero() {
		where = "fechaextraccion between '" +
			strings.Split(filtro.FechaInicio.String(), " ")[0] +
			"' and '" + strings.Split(filtro.FechaFinal.String(), " ")[0] + "' "
	}
	if filtro.IdProducto != "" {
		where += "and sku = '" + filtro.IdProducto + "' "
	}
	if len(filtro.Claves) != 0 {
		for i := 0; i < len(filtro.Claves); i++ {
			if i == 0 {
				where += "and (claveAlmacen = '" + filtro.Claves[i] + "' "
			} else {
				where += "or claveAlmacen = '" + filtro.Claves[i] + "' "
			}
		}
		where += ") "
	}
	if len(where) != 0 {
		if strings.Split(where, " ")[0] == "and" {
			where = where[4:]
		}
		where = "where " + where
	}
	return where
}

func (c Consultas) DatosFrecuencias(filtro *Model.Filtro) (*Model.TablaFrecuencias, error) {
	//hacemos una conexion a la base de datos
	db := Get()
	defer db.Close()

	var tablaFrecuencias Model.TablaFrecuencias

	listaSkus, err := c.ObtenerSkus(filtro)
	if err != nil {
		return nil, err
	}

	for i := 0; i < len(listaSkus); i++ {
		var listaFrecuencias Model.ListaFrecuencia
		filtro.IdProducto = listaSkus[i]
		where := WhereGenerico(filtro)
		query := "SELECT sku, claveAlmacen, fechaextraccion, sum(ticket), sum(ventas) FROM datos_kore.abc " +
			where +
			"group by sku, fechaextraccion, claveAlmacen order by sku"
		stmt, err := db.Prepare(query)
		if err != nil {
			return nil, err
		}
		rows, err := stmt.Query()
		if err != nil {
			return nil, err
		}
		for rows.Next() {
			var frecuencia Model.Frecuencia
			err = rows.Scan(&frecuencia.IdProducto, &frecuencia.ClaveAlmacen, &frecuencia.Fecha, &frecuencia.Frecuencia, &frecuencia.SumaPrecioVentas)
			if err != nil {
				return nil, err
			}
			listaFrecuencias.Agregar(frecuencia)
		}
		tablaFrecuencias.AgregarFila(&listaFrecuencias)
	}
	filtro.IdProducto = ""
	return &tablaFrecuencias, nil
}

func (c Consultas) ObtenerRangoFechas(filtro *Model.Filtro) error {
	//hacemos una conexion a la base de datos
	db := Get()
	defer db.Close()

	filtro.FechaInicio = time.Time{}
	filtro.FechaFinal = time.Time{}
	where := WhereGenerico(filtro)

	query := "SELECT MAX(fechaextraccion) AS fechaMax, MIN(fechaextraccion) AS fechaMin " +
		"FROM datos_kore.abc " +
		where

	stmtTiquets, err := db.Prepare(query)
	if err != nil {
		return err
	}
	defer stmtTiquets.Close()

	row := stmtTiquets.QueryRow()
	row.Scan(&filtro.FechaFinal, &filtro.FechaInicio)
	return nil
}

func (c Consultas) ObtenerSkus(filtro *Model.Filtro) ([]string, error) {
	db := Get()
	defer db.Close()

	var skus []string

	where := WhereGenerico(filtro)

	query := "select sku from datos_kore.abc " + where + "group by sku order by sku"
	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var id string
		rows.Scan(&id)
		skus = append(skus, id)
	}
	return skus, nil
}

func (c Consultas) ObtenerClaveAlmacenes(filtro *Model.Filtro) ([]string, error) {
	db := Get()
	defer db.Close()

	var claveAlmacenes []string

	where := WhereGenerico(filtro)

	query := "select claveAlmacen from datos_kore.abc " + where + "group by claveAlmacen order by claveAlmacen"
	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var id string
		rows.Scan(&id)
		claveAlmacenes = append(claveAlmacenes, id)
	}
	return claveAlmacenes, nil
}

func (c Consultas) ObtenerAlmacenes(filtro *Model.Filtro) ([][2]string, error) {
	db := Get()
	defer db.Close()

	var almacenes [][2]string

	query := "select claveAlmacen, nombreAlmacen from datos_kore.abc group by claveAlmacen, nombreAlmacen "
	stmt, err := db.Prepare(query)
	if err != nil {
		return nil, err
	}
	rows, err := stmt.Query()
	if err != nil {
		return nil, err
	}
	for rows.Next() {
		var almacen [2]string
		rows.Scan(&(almacen[0]), &(almacen[1]))
		almacenes = append(almacenes, almacen)
	}
	return almacenes, nil
}
