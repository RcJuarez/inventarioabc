package motoresDB

type Config struct {
	Engine   string
	Server   string
	Port     string
	Dbname   string
	User     string
	Password string
	SSLmode  string
}
