package factory

/*
	@Modulo	Es una fabrica de objetos de una base de datos.
					Crea un objeto concreto a partir de una tabla y
					volcarlo sobre una estructura para su manipulacion
*/

import (
	"log"

	"../interfaces"
	"../postgres"
)

/*
	@funcs 	Las funciones descritas a continuacion permiten devolver un objeto con base en
	una tabla dentro de la base de datos, solo le indicamos que motor queremos usar y
	y devuelve un objeto con los datos obtenidos
*/

//De la Tabla Tiquet obtenemos un tiquet{}
func Consultas(engine string) interfaces.IFrecuencia {
	var manejadorTiquet interfaces.IFrecuencia
	switch engine {
	case "postgres":
		manejadorTiquet = postgres.Consultas{}
	default:
		log.Fatalf("El motor no esta implementado %s", engine)
	}

	return manejadorTiquet
}
